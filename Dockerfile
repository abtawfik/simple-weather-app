FROM python:3.6

MAINTAINER Ahmed Tawfik

RUN apt-get update -y

COPY requirements.txt /app/
COPY ./app /app
WORKDIR /app

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5000

ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:5000", "-w", "1", "wsgi:server"]
