#!/bin/bash


#=============================================
#== Completely clean up the weather app info
#== from Docker's perspective
#=============================================
if [ $1 == 'clean' ]; then
    docker stop weather_app
    docker rm weather_app
    docker rmi -f simple_wx_app
    docker image prune -f
fi



#=============================================================
#== Build the Docker image and call it 'simple_wx_app'
#== Next run the container and name it 'weather_app'
#== Finally output the log to see if it is working properly
#== You can terminate the log using CTL+C
#=============================================================
if [ $1 == 'run' ]; then
    docker build -t simple_wx_app .
    docker run --name weather_app -d -p 1500:5000 simple_wx_app
    docker logs -tf weather_app
fi

