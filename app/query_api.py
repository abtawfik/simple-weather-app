#======================================================
#===
#===
#===  Stores the functions for requesting and parsing
#===  responses from yahoo api
#===
#===  Check out https://developer.yahoo.com/ for weather api
#===  Check out https://developers.google.com/console for address to lat/lon
#===
#======================================================
import os
import json
import base64
import requests
import googlemaps
import pandas as pd
from tokens import *
from datetime import datetime, timedelta


#------------------------------------------------------
#--  Invoke Google API to convert lat/lon to address
#------------------------------------------------------
def address_to_lat_lon(address):
    '''
    Converts the user-input address into lat/lon
    that way we can use it for the yahoo weather API

    Inputs
       address:  A user defined address or zip code
    Outputs
       lat:  Latitude in degrees East
       lon:  Longitude in degrees North
    '''
    gmaps    =  googlemaps.Client(key=keys)
    geocode  =  gmaps.geocode(address)
    return geocode[0]['geometry']['viewport']['northeast']['lat'], geocode[0]['geometry']['viewport']['northeast']['lng']


#----------------------------------------------
#--  Get data from query.yahooapis.com
#----------------------------------------------
def get_by_lat_lon(lat,lon):
    '''
    Makes a request the yahoo weather api using lat and lon
    instead of the woeid

    Inputs
       lat:  Latitude in degrees East
       lon:  Longitude in degrees North

    Outputs
       results:  a python dictionary with a forecast and current
                 weather conditions
    '''
    # Base endpoint for the yahoo api as of June 2018
    baseurl = "https://query.yahooapis.com/v1/public/yql"

    # Specify the query to retrieve by lat/lon instead of the default woeid
    query = "select * from weather.forecast where woeid in "+ \
            "(SELECT woeid FROM geo.places WHERE text = '({}, {})')"

    # Parameters that will be submitted to the API and return a json
    params = {'q': query.format(lat,lon), 'format': 'json'}

    # Return the request as a JSON
    result = requests. get ( baseurl, params = params )
    return result.json()





#----------------------------------------------
#--  Parse the API request
#----------------------------------------------
def parse_forecast(yahoo_response):
    '''
    Parse the incoming api response forecast and current conditions
    into two pandas dataframes
    
    Inputs
        yahoo_response:  the yahoo api dictionary response returned from
                         get_by_lat_lon()

    Outputs
        current:  the current weather conditions temp/humidity/pressure/visibility/conditions
        forecast: the daily highs/lows/day of week/and weather conditions
        location: location information city, region(state), country
    '''
    location     =                 yahoo_response['query']['results']['channel']['location']
    lat          =                 yahoo_response['query']['results']['channel']['item']['lat']
    lon          =                 yahoo_response['query']['results']['channel']['item']['long']
    current      =   pd.DataFrame( yahoo_response['query']['results']['channel']['item']['condition'], index=[0]  )
    current.join( pd.DataFrame(    yahoo_response['query']['results']['channel']['atmosphere']       , index=[0]) )
    forecast     =   pd.DataFrame( yahoo_response['query']['results']['channel']['item']['forecast' ] ).set_index('date')
    now          =   datetime.now()
    now          =   pd.to_datetime(now)
    try:
        sunrise      =   pd.to_datetime(yahoo_response['query']['results']['channel']['astronomy']['sunrise'])
        sunset       =   pd.to_datetime(yahoo_response['query']['results']['channel']['astronomy']['sunset'])
        is_it_night  =  (now < sunrise) | (now > sunset)
    except:
        sunrise      =   pd.to_datetime(now.date()) + timedelta(hours=7)
        sunset       =   pd.to_datetime(now.date()) + timedelta(days =1) - timedelta(hours=7)
        is_it_night  =  (now < sunrise) | (now > sunset)
    return (current, forecast, location, is_it_night, lat, lon)



#----------------------------------------------
#--  Combines the parse_forecast and api_call
#----------------------------------------------
def query_and_parse(lat,lon):
    '''
    A wrapper for the two functions above. That way I only have to make 
    one call and retreive the current state and forecast    
    Inputs
       lat:  Latitude in degrees East
       lon:  Longitude in degrees North

    Outputs
        current:  the current weather conditions temp/humidity/pressure/visibility/conditions
        forecast: the daily highs/lows/day of week/and weather conditions
        location: location information city, region(state), country
    '''
    return parse_forecast(get_by_lat_lon(lat,lon))


#--------------------------------------------------------------------------------
#--  Pack data from api call into a json for storing; stored in ID = store-data
#--------------------------------------------------------------------------------
def pack_to_json(data):
    return data[0].to_json()+'$'+data[1].to_json()+'$'+json.dumps(data[2])+'$'+  \
           json.dumps(data[3])+'$'+json.dumps(data[4])+'$'+json.dumps(data[5])


#------------------------------------------------------
#--  Unpacks the json containing current weather data
#------------------------------------------------------
def unpack_json(packed_info):
    packed  =  packed_info.split('$')
    return pd.read_json(packed[0]), pd.read_json(packed[1]), json.loads(packed[2]), \
           json.loads(packed[3]), json.loads(packed[4]), json.loads(packed[5])

#------------------------------------------------------
#--  Encode image to serve
#------------------------------------------------------
def get_weather_image(weather_code, suffix='.png'):
    image          =  os.getcwd() + '/colorful_png/' + str(weather_code) + suffix
    encoded_image  =  base64.b64encode(open(image, 'rb').read())
    return 'data:image/{};base64,{}'.format(suffix,encoded_image.decode())
