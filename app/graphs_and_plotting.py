###########################################################
###
###  Functions that plot things
###
###########################################################
import plotly.graph_objs as go
from tokens import *

#-------------------------------------------------------------
#  Generates mapbox plot
#-------------------------------------------------------------
def create_map(lat,lon):
    data   = [go.Scattermapbox( lat=[lat], lon=[lon],mode='markers',
                                marker=dict(size=14), text=['Montreal'])]
    layout = go.Layout( autosize=False, width=400, height=200, hovermode='closest',
                        margin={'r':0, 'b':0, 'l':0, 't':0},
                        mapbox=dict( accesstoken=mapbox_access_token,
                                     bearing=0,
                                     center=dict(lat=lat,lon=lon),
                                     pitch=0,
                                     zoom=7),)
    return dict( data=data, layout=layout )


#-------------------------------------------------------------
#  Remove the tooltips from the plotly graph 
#  Can be used on any graph so placed outside of functions
#  Used for the config inside the dcc.Graph() (see components.py)
#-------------------------------------------------------------
buttons_to_remove  =  [ "zoom2d", "pan2d", "select2d", "lasso2d", "zoomIn2d", 
                        "zoomOut2d", "autoScale2d", "resetScale2d",
                        "hoverClosestCartesian",
                        "zoom3d", "pan3d", "resetCameraDefault3d", 
                        "resetCameraLastSave3d", "hoverClosest3d",
                        "orbitRotation", "tableRotation",
                        "zoomInGeo", "zoomOutGeo", "resetGeo", "hoverClosestGeo",
                        "toImage", "sendDataToCloud",
                        "hoverClosestGl2d",
                        "hoverClosestPie",
                        "toggleHover",
                        "resetViews",
                        "toggleSpikelines",
                        "resetViewMapbox"
                        ]
remove_some_tools  = {'displaylogo':False, 'modeBarButtonsToRemove':buttons_to_remove}

#-------------------------------------------------------------
#  Generates plotly line plots given an input dictionary
#-------------------------------------------------------------
def generate_line_plot(df, columns=['high','low'], colors=['#85C1E9', '#E59866']):

    #----------------------------------------------------------------------------
    #  Time is handled different in plotting options: time axis versus standard
    #
    #  y-axis: time axis or not?
    #----------------------------------------------------------------------------
    yaxis_layout  =  {'title': 'Temperature [F]', 'type':'', 'autorange': True, 'showgrid': True,'mirror':True}
    #----------------------------------------------------------------------------
    #  x-axis: time axis
    #----------------------------------------------------------------------------
    xaxis         =  df.index
    xaxis_layout  =  {'title': 'Time', 'type': 'date', 'autorange': True, 'showgrid': True, 'mirror':True}

    #----------------------------------------------------------------------------
    #  Create the lines
    #----------------------------------------------------------------------------
    lines  =  []
    for col in columns:
        marker_dict  =  {'size' : 8  , 'color':'','line': {'width': 0.2, 'color': 'grey'}}  
        line_dict    =  {'width': 6.5, 'color':'' }
        lines.append( go.Scatter(x=xaxis, y=df[col], mode='lines+markers',
                                 opacity=0.9, marker=marker_dict, line=line_dict, 
                                 name=col))
                 
    layout  =  go.Layout(xaxis=xaxis_layout, yaxis=yaxis_layout,
                         legend={'x': 0, 'y': 1}, hovermode='closest',
                         title = '10-day forecast', paper_bgcolor='#D5DBDB',
                         plot_bgcolor='#D5DBDB')

    return {'data': lines,  'layout': layout}


