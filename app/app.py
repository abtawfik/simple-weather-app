########################################################
###
###
###
###  
###
###
########################################################
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

from query_api import *
from graphs_and_plotting import *
from components import Entire_Page


#=======================================================================
#===  Creates the dash instance 
#=======================================================================
app    = dash.Dash(port=5000)
server = app.server
app.config.suppress_callback_exceptions = True


#=======================================================================
#===  Layout and pulls the layout from components.py
#=======================================================================
app.layout = Entire_Page


#=======================================================================
#===  Callbacks section -- describes the interactivity of the dashboard
#===  e.g. how components (from components.py) interact with one another
#=======================================================================
#---------------------------------------------------------------------------------------
#   Store data in a hidden child div that way we don't have to make multiple calls to
#   the API everytime someone pushes the button...this is because DASH only allows 
#   one output per callback
#---------------------------------------------------------------------------------------
@app.callback(Output('store-data' , 'children' ) ,
              [Input('submit'     , 'n_clicks' )],
              [State('address'    , 'value'    )])
def update_location_text(clicks,address):
    if clicks is None:
        return pack_to_json(query_and_parse(35.9940, -78.8986))
    lat, lon  =  address_to_lat_lon(address)
    return pack_to_json(query_and_parse(lat,lon))



#---------------------------------------------------------------------------------------
#   Update Map
#---------------------------------------------------------------------------------------
@app.callback(Output('map'        , 'figure'  ) ,
              [Input('store-data' , 'children')])
def update_map(data):
    if data is None:
        return create_map(35.994, -78.8986)
    current, forecast, place, is_it_night, lat, lon  =  unpack_json(data)
    return create_map( lat, lon )



#---------------------------------------------------------------------------------------
#   Update Current Weather Condition Image
#---------------------------------------------------------------------------------------
@app.callback(Output('current-wx-image'  , 'src'      ) ,
              [Input('store-data'        , 'children' )])
def update_valid_date(data):
    if data is None:
        return None
    current, forecast, place, is_it_night, lat, lon  =  unpack_json(data)
    return get_weather_image(current['code'].values[0])



#---------------------------------------------------------------------------------------
#   Update Forecast Weather
#---------------------------------------------------------------------------------------
@app.callback(Output('forecast-timeseries' , 'figure'   ) ,
              [Input('store-data'          , 'children' )])
def update_valid_date(data):
    if data is None:
        return None
    current, forecast, place, is_it_night, lat, lon  =  unpack_json(data)
    return generate_line_plot(forecast)




#---------------------------------------------------------------------------------------
#   Update Valid Date
#---------------------------------------------------------------------------------------
@app.callback(Output('valid-date' , 'children' ) ,
              [Input('store-data' , 'children' )])
def update_valid_date(data):
    if data is None:
        return None
    current, forecast, place, is_it_night, lat, lon  =  unpack_json(data)
    return 'Valid time:  '+str(current['date'].values[0])



#---------------------------------------------------------------------------------------
#   Update Current Temperature
#---------------------------------------------------------------------------------------
@app.callback(Output('current-temp', 'children' ) ,
              [Input('store-data'  , 'children' )])
def update_valid_date(data):
    if data is None:
        return None
    current, forecast, place, is_it_night, lat, lon  =  unpack_json(data)
    return str(current['temp'].values[0])



#---------------------------------------------------------------------------------------
#   Update Current Weather Conditions
#---------------------------------------------------------------------------------------
@app.callback(Output('current-text', 'children' ) ,
              [Input('store-data'  , 'children' )])
def update_valid_date(data):
    if data is None:
        return None
    current, forecast, place, is_it_night, lat, lon  =  unpack_json(data)
    return str(current['text'].values[0])


#---------------------------------------------------------------------------------------
#   Update Location
#---------------------------------------------------------------------------------------
@app.callback(Output('location'   , 'children' ) ,
              [Input('store-data' , 'children' )])
def update_valid_date(data):
    if data is None:
        return None
    current, forecast, place, is_it_night, lat, lon  =  unpack_json(data)
    return place['city']+', '+place['region']+' '+place['country']




#========================================================================================
#===  External CSS and JS styles from Materialize CSS -- makes things pretty
#========================================================================================
external_css = ["https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"]
for css in external_css:
    app.css.append_css({"external_url": css})

external_js = ['https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js']
for js in external_js:
    app.scripts.append_script({'external_url': js})


#========================================================================================
#===
#===  Main app call
#===  UNCOMMENT THIS TO RUN LOCALLY (python app.py)
#=== 
#===  LEAVE COMMENTED WHEN USING DOCKER RUN
#===
#========================================================================================
if __name__ == '__main__':
    app.run_server(debug=True)

