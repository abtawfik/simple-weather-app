########################################################
###
###  HTML Layout and Components:
###
###  Define some colors, grids, styles, and all the
###  interactive components.
###  This uses Materialize CSS formatting
###
########################################################
from query_api import *
from graphs_and_plotting import *
from dash.dependencies import Input, Output, State, Event
import dash_core_components as dcc
import dash_html_components as html



#=================================================================
#===  Define some colors, grids, and styles 
#===  This uses Materialize CSS formatting
#=================================================================
background_color    =  'blue-grey darken-1'
title_color         =  'indigo-text text-lighten-4'
body_column_length  =  'col s12'
body_class          =  background_color
header_class        =  'orange-text text-lighten-5'
temp_class          =  'brown-text text-lighten-5'

no_padding          =  {'padding':0, 'margin':0}
negative_padding    =  {'padding':0, 'margin':0, 'padding-bottom':-10}
smaller_text        =  {}


#=================================================================
#===  Define some helper functions and typical divs
#=================================================================
def create_div(divs_inside, myclass):
    return html.Div( divs_inside, className=myclass )

def create_row(divs_inside, style=no_padding, append=''):
    return html.Div( divs_inside, style=style, className='row '+append )

br       =  create_div(html.Br(), 'row' )
Divider  =  create_div('', 'divider')


#-------------------------------------------------------------------------------------
# Set a default value for latitude and longitude
#  ---> Bull City represent <---
#-------------------------------------------------------------------------------------
lat, lon            =  35.9940, -78.8986
current, forecast,  \
place, is_it_night, \
lat, lon            =  query_and_parse(lat,lon)
city_state_country  =  place['city']+', '+place['region']+' '+place['country']
date                =  str(current['date'].values[0])


#-------------------------------------------------------------------------------------
#  Component that holds the data from the API Call
#-------------------------------------------------------------------------------------
Store_Data         =  html.Div (id='store-data' , style={'display': 'none'})


#-------------------------------------------------------------------------------------
#
#                                 Top Section
#
#     1)  'Current Weather' title text                  |
#     2)  Selected location name 'City, State Country'  |   4) Address input box 5) Button
#     3)  When was the forecast valid for [time stamp]  |          
#
#
#-------------------------------------------------------------------------------------
# Component (1) #
Current_Weather_Text  =  create_div( html.H3('Current Weather', id='current-condition-content', 
                                             style={'font-weight':'lighter'}), header_class)
# Component (2) #
Location              =  create_div( html.H5(city_state_country, id='location'  ), header_class)

# Component (3) #
Valid_for             =  html.Div( html.H6('Valid time:  '+date, id='valid-date', style=no_padding, 
                                           className='left-align'), className=header_class, style=no_padding)
# Component (4) #
Address            =  create_div(dcc.Input(id='address'  , placeholder='zip code...'                    ), 'col s8')

# Component (5) #
Go_Button          =  create_div(html.Button('Go', className='btn waves-effect waves-light', id='submit'), 'col s4')
Lat_Lon_Container  =  create_div( [Address, Go_Button], 'col s12')
Search_Stuff       =  create_div( [create_row(br), create_row(br),
                                   create_row(Lat_Lon_Container, style=negative_padding, append='right')], 'col s6' )


Above_the_first_divider  =   create_div( [create_row([Current_Weather_Text]),
                                          create_row([Location            ]),
                                          create_row([Valid_for           ])], 'col s6'  )
Title_and_Search_Bar     =   create_div( [Above_the_first_divider,Search_Stuff], 'col s12' )



#-------------------------------------------------------------------------------------
#
#                                 Middle Section
#
#     1)  Image of Current Weather Conditions         | 
#     2)  Text below image describing current weather |    4) Map of selected location
#     3)  Current temperature                         |
#                                                     |
#
#-------------------------------------------------------------------------------------
# Component (1) #
Current_Wx_Image            =   create_div( html.Img(src=get_weather_image(current['code'].values[0]), 
                                                     id='current-wx-image'), 'col s6 center-align')

# Component (2) #
Current_Wx_Condition        =   create_div( create_div( html.H5(str(current['text'].values[0]), 
                                                                style={'font-weight':'lighter'}, id='current-text'), 
                                                        temp_class),'col s6 center-align')
# Component (3) #
Current_Temp                =   create_div( create_div( html.H3(str(current['temp'].values[0]), id='current-temp'), 
                                                        temp_class), 'col s6 left valign-wrapper')
# Component (4) #
Map                         =   create_div( dcc.Graph(id='map', figure=create_map(lat,lon)), 'col s4 center')

# Create space above temperature value for aesthetic
Row_of_Temperature          =   create_div( [create_row(br), Current_Temp], 'col s6')

# Combine image, text, and temp in half page div
Current_Temp_and_Image_Box  =   create_div( [create_row([Current_Wx_Image,Row_of_Temperature]),
                                             create_row([Current_Wx_Condition               ])], 'col s6'  )

# Put all the middle section elements together
Current_Wx_and_Map          =   create_div( [Current_Temp_and_Image_Box,Map], 'col s12' )




#-------------------------------------------------------------------------------------
#  Forecast Timeseries graph of high and low temperatures
#-------------------------------------------------------------------------------------
Forecast_Section   =  create_row( dcc.Graph(id='forecast-timeseries', 
                                            figure=generate_line_plot(forecast),
                                            config=remove_some_tools), append='center-align' )


#-------------------------------------------------------------------------------------
#  Layout of the Main body organized sequentially
#-------------------------------------------------------------------------------------
body_list    =  []
body_list.append   ( create_div( br                            , 'row' ) )
body_list.append   ( create_div( Title_and_Search_Bar          , 'row' ) )
body_list.append   ( create_div( Divider                       , 'row' ) )
body_list.append   ( create_div( Current_Wx_and_Map            , 'row' ) )
body_list.append   ( create_div( Divider                       , 'row' ) )
body_list.append   ( create_div( Forecast_Section              , 'row' ) )
body_list.append   ( create_div( br                            , 'row' ) )
body_list.append   ( create_div( br                            , 'row' ) )
body_rows    =  create_div(body_list, 'container')
Main_Body    =  create_div(body_rows, body_class )



#-------------------------------------------------------------------------------------
#  The final layout of the page:  This is used in main.py and passed to app.layout
#-------------------------------------------------------------------------------------
Entire_Page  =  create_div([Main_Body,Store_Data], myclass='row')



